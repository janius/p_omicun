<?php

namespace modules\controllers;
use \Controller;

class MainController extends Controller {

    protected function template($templateName, $headerViewName, $contentViewName, $footerViewName, $headerData = array(), $contentData = array(), $footerData = array()) {

        $view = $this->view($templateName);

        $page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : '';

        $view->bind('headerViewName', $headerViewName);
        $view->bind('contentViewName', $contentViewName);
        $view->bind('footerViewName', $footerViewName);

        $view->bind('headerData', $headerData);
        $view->bind('contentData', $contentData);
        $view->bind('footerData', $footerData);
    }

}
?>