<?php

use \modules\controllers\MainController;

class ProgramController extends MainController {

  public function detailProgramInHouse() {

    $this->model('ProgramInhouse');

    $slug = $_GET["slug"];

    $dataProgramInHouse = $this->ProgramInhouse->getWhere(array(
        'programNameSlug' => $slug
      ), '', 'AND', '');

  	$this->template('tmplt', 'hdr_color', 'c_program_detail', 'ftr', array(), 
		array(
  		 	'title' => '',
        'program' => $dataProgramInHouse[0]
  		),
	   array());    
  }

  public function detailProgramPublic() {

    $this->model('ProgramPublic');

    $slug = $_GET["slug"];

    $dataProgramPublic = $this->ProgramPublic->getWhere(array(
        'programNameSlug' => $slug
      ), '', 'AND', '');

    $this->template('tmplt', 'hdr_color', 'c_program_detail', 'ftr', array(), 
    array(
        'title' => '',
        'program' => $dataProgramPublic[0]
      ),
     array());    
  }
}
?>