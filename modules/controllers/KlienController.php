<?php

use \modules\controllers\MainController;

class KlienController extends MainController
{

  public function allClient()
  {

    $this->model('Client');

    $dataClient = $this->Client->getWhere(
      '',
      array(
        'clientName' => 'ASC',
      ), 'AND', '');

    $this->template('tmplt', 'hdr_color', 'c_client_all', 'ftr', array(),
      array(
        'title'  => '',
        'client' => $dataClient,
      ),
      array());
  }
}