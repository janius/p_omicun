<?php

use \modules\controllers\MainController;

class BerandaController extends MainController {

  public function index() {

    $this->model('Article');
    $this->model('Banner');
    $this->model('Client');
    $this->model('Contact');
    $this->model('ProfileInformation');
    $this->model('ProfileImage');
    $this->model('ProgramInhouse');
    $this->model('ProgramPublic');

    $dataArticle = $this->Article->get();
    $dataBanner = $this->Banner->get();
    $dataClient = $this->Client->getWhere(array(
        'top' => true
      ), '', 'AND', '');
    $dataContact = $this->Contact->get()[0];
    $dataProfileInformation = $this->ProfileInformation->get()[0];
    $dataProfileImage = $this->ProfileImage->get();
    $dataProgramInhouse = $this->ProgramInhouse->get();
    $dataProgramPublic = $this->ProgramPublic->get();

  	$this->template('tmplt', 'hdr', 'c_home', 'ftr', array(), 
		array(
  		 	'title' => '',
        'article' => $dataArticle,
        'banner' => $dataBanner,
        'client' => $dataClient,
        'contact' => $dataContact,
        'profile_information' => $dataProfileInformation,
        'profile_image' => $dataProfileImage,
        'program_inhouse' => $dataProgramInhouse,
        'program_public' => $dataProgramPublic
  		),
	   array());    
  }


  public function saveMessage() {

    $this->model('IncomingMessage');

    $name = $_POST["name"];
    $email = $_POST["email"];
    $message = $_POST["message"]; 

    $insert = $this->IncomingMessage->insert(
      array(
        'name'   => $name,
        'email'   => $email,
        'message'   => $message
      )
    );  

    if($insert) {

      header('Location: '.PATH);
      break;
    } 

  }
}
?>