<?php

use \modules\controllers\MainController;

class ArtikelController extends MainController {

  public function detailArtikel() {

    $this->model('Article');

    $slug = $_GET["slug"];

    $dataArticle = $this->Article->getWhere(array(
        'articleTitleSlug' => $slug
      ), '', 'AND', '');

  	$this->template('tmplt', 'hdr_color', 'c_blog_detail', 'ftr', array(), 
		array(
  		 	'title' => '',
        'article' => $dataArticle[0]
  		),
	   array());    
  }
}
?>