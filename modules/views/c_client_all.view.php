<section class="section">
  <div class="container">
    <div class="headline">
      <h1 class="section-title">Semua Klien</h1>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Perusahaan</th>
            <th>Bidang</th>
            <th>Training / Consulting</th>
          </tr>
        </thead>
        <tbody>
        <?php 
          $no = 1;
          foreach($contentData["client"] as $client) {
        ?>
          <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $client->clientName ?></td>
            <td><?php echo $client->clientBusinessLine ?></td>
            <td><?php echo $client->clientService ?></td>
          </tr>
        <?php 
          }
        ?>
        </tbody>
      </table>
      <hr />
      <a href="<?php echo PATH ?>" class="btn btn-default" role="button"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp; Kembali</a>
    </div>
  </div>
</section>