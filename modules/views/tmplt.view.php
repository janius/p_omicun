<?php
    $page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : '';
?>
<!DOCTYPE html>
<html lang="en">
<!-- 
Developed by :   

             ██╗ █████╗ ███╗   ██╗██╗██╗   ██╗███████╗   ██╗██████╗ 
             ██║██╔══██╗████╗  ██║██║██║   ██║██╔════╝   ██║██╔══██╗
             ██║███████║██╔██╗ ██║██║██║   ██║███████╗   ██║██║  ██║
        ██   ██║██╔══██║██║╚██╗██║██║██║   ██║╚════██║   ██║██║  ██║
        ╚█████╔╝██║  ██║██║ ╚████║██║╚██████╔╝███████║██╗██║██████╔╝
         ╚════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚══════╝╚═╝╚═╝╚═════╝ 
 __      __   _      ___  _      _ _        _    ___              _   _         
 \ \    / /__| |__  |   \(_)__ _(_) |_ __ _| |  / __|_ _ ___ __ _| |_(_)_ _____ 
  \ \/\/ / -_) '_ \ | |) | / _` | |  _/ _` | | | (__| '_/ -_) _` |  _| \ V / -_)
   \_/\_/\___|_.__/ |___/|_\__, |_|\__\__,_|_|  \___|_| \___\__,_|\__|_|\_/\___|
                           |___/                                                
-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Productivity Excellence</title>
  <meta name="description" content="">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" />
  <link href="ast/c/style.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800&amp;subsetting=all' rel='stylesheet' type='text/css'>
</head>

<body data-spy="scroll" data-target="#header">

    <?php

    if($headerViewName != '') {
      $headerView = new View($headerViewName);
      $headerView->bind('headerData', $headerData);
      $headerView->forceRender();
    }

    $contentView = new View($contentViewName);
    $contentView->bind('contentData', $contentData);
    $contentView->forceRender();

    if($footerViewName != '') {
      $footertView = new View($footerViewName);
      $footertView->bind('footerData', $footerData);
      $footertView->forceRender();
    }
  ?>
  
  <a href="#" class="scroll-up"><i class="fa fa-arrow-up"></i></a>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
  <script type="text/javascript">
  window.jQuery || document.write('<script src="ast/j/jquery-2.1.0.min.js"><\/script>')
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js"></script>
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyDEXAdJmkOm__OQFQDRCBEUYctAYxa9l24"></script>
  <script src="ast/j/gmaps.js"></script>
  <script src="ast/j/custom.js" type="text/javascript"></script>
</body>

</html>