<section class="section">
  <div class="container">
    <div class="headline">
      <h1 class="section-title"><?php echo $contentData['article']->articleTitle ?></h1>
      <img src="public/article/<?php echo $contentData['article']->articleImage ?>" alt="" class="img-responsive" title="">
    </div>
    <?php echo $contentData['article']->articleDetail ?>
    <hr />
    <a href="<?php echo PATH ?>" class="btn btn-default" role="button"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp; Kembali</a>
  </div>
</section>