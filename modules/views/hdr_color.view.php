<header id="header">
  <nav class="navbar navbar-inverse navbar-fixed-top color" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand text-logo" href="<?php echo PATH ?>"><img class="img-responsive" style="height: 100%;" src="ast/i/pe-logo-text.png" /></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav menu-right">
          <li class="active"><a href="#header">Beranda</a></li>
          <li><a href="#about">Tentang Kami</a></li>
          <li><a href="#program">Program</a></li>
          <li><a href="#client">Klien</a></li>
          <li><a href="#artikels">Artikel</a></li>
          <li><a href="#contact">Hubungi Kami</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>