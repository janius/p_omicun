<div id="main-slider" class="carousel slide carousel-fade">
  <div class="carousel-inner">
    <?php
      $index = 0;
      foreach($contentData["banner"] as $banner) {
    ?>
      <div class="item cover <?php echo ($index == 0) ? 'active' : ''?>" style="background-image: url(<?php echo PATH ?>public/banner/<?php echo $banner->image ?>)">
        <div class="carousel-caption">
          <h2><?php echo $banner->title ?></h2>
        </div>
      </div>
      <?php 
    $index++;
    } ?>
  </div>
  <div class="overlay-bg"></div>
  <a class="left carousel-control" href="#main-slider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" href="#main-slider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
<section id="about" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 headline">
        <h1>Tentang Kami</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-6">
        <div class="carousel slide carousel-mod" data-ride="carousel" id="side-carousel">
          <div class="carousel-inner">
            <?php
              $index = 0;
              foreach($contentData["profile_image"] as $profile_image) {
            ?>
              <div class="item <?php echo ($index == 0) ? 'active' : ''?>">
                <img width="100%" src="<?php echo PATH ?>public/profile/<?php echo $profile_image->image ?>" alt="" title="" />
              </div>
              <?php 
            $index++;
            } ?>
          </div>
          <ol class="carousel-indicators">
            <li data-target="#side-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#side-carousel" data-slide-to="1"></li>
            <li data-target="#side-carousel" data-slide-to="2"></li>
          </ol>
        </div>
      </div>
      <div class="col-md-6 col-md-6">
        <?php echo $contentData["profile_information"]->description ?>
      </div>
    </div>
  </div>
</section>
<section id="program" class="section" style="background-color: #fcfcfc">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="headline">
          <h1 class="section-title">Program</h1>
        </div>
      </div>
    </div>
    <h3 class="section-description text-center" style="margin-bottom: 40px;margin-top: 20px;">Program Public Training</h3>
    <div class="row equal">
      <?php
        foreach($contentData["program_public"] as $program_public) {
      ?>
        <div class="col-xs-6 col-md-3 program">
          <a href="program-public-<?php echo $program_public->programNameSlug ?>">
            <div class="services-list">
              <h4><?php echo $program_public->programName ?></h4>
            </div>
          </a>
        </div>
        <?php } ?>
    </div>
    <h3 class="section-description text-center" style="margin-bottom: 40px;margin-top: 20px;">Program In-House Training</h3>
    <div class="row equal">
      <?php
        foreach($contentData["program_inhouse"] as $program_inhouse) {
      ?>
        <div class="col-xs-6 col-md-3 program">
          <a href="program-inhouse-<?php echo $program_inhouse->programNameSlug ?>">
            <div class="services-list">
              <h4><?php echo $program_inhouse->programName ?></h4>
            </div>
          </a>
        </div>
        <?php } ?>
    </div>
  </div>
</section>
<section id="client" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="headline">
          <h1 class="section-title">Klien Kami</h1>
        </div>
      </div>
    </div>
    <div class="row">
      <div class='col-md-12'>
        <div class="client-slick">
          <?php
            foreach($contentData["client"] as $client) {
          ?>
            <div>
              <div class="client-inner">
                <img src="public/client/<?php echo $client->clientImage ?>" class="img-responsive" alt="" title="" />
              </div>
            </div>
            <?php } ?>
        </div>
      </div>
    </div>
    <hr />
    <div class="view-all">
      <a href="semua-klien" class="btn btn-danger" role="button">Lihat Semua</a>
    </div>
</section>
<section id="artikels" class="section" style="background-color: #fcfcfc">
  <div class="container">
    <div class="headline">
      <h1 class="section-title">Artikel</h1>
    </div>
    <div class="blog-slick">
      <?php
        foreach($contentData["article"] as $article) {
      ?>
        <div>
          <a href="artikel-<?php echo $article->articleTitleSlug ?>">
            <div class="blog-holder">
              <div class="blog-detail">
                <img src="public/article/<?php echo $article->articleImage ?>" alt="" class="img-responsive" title="">
                <h3><?php echo $article->articleTitle ?></h3>
                <?php echo $article->articleDetail ?>
              </div>
            </div>
          </a>
        </div>
        <?php } ?>
    </div>
    <hr />
    <div class="view-all">
      <a href="semua-artikel" class="btn btn-danger" role="button">Lihat Semua</a>
    </div>
</section>
<section id="contact" class="section">
  <div class="container">
    <div class="row">
      <div class="headline">
        <h1>Hubungi Kami</h1>
      </div>
    </div>
    <div id="map" data-lat="<?php echo $contentData[" contact "]->mapLat?>" data-long="<?php echo $contentData[" contact "]->mapLong?>"></div>
    <div class="row">
      <div class="col-sm-5 col-md-4">
        <div class="contact-info">
          <h3>Kontak</h3>
          <ul class="contact-list">
            <li><strong>Alamat :</strong></li>
            <li>
              <?php echo $contentData["contact"]->address ?>
            </li>
            <li><strong>Nomor Telepon :</strong></li>
            <li>
              <?php echo $contentData["contact"]->mobileNumber ?>
            </li>
            <li><strong>Email :</strong></li>
            <li>
              <?php echo $contentData["contact"]->email ?>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-7 col-md-8">
        <form id="contact_form" class="contact-form" action="save-pesan" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" name="name" id="name" type="text" placeholder="Nama" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" name="email" id="email" type="email" placeholder="Email" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="message" id="message" placeholder="Pesan" required></textarea>
              </div>
              <button type="submit" name="contact-submit" id="submit_contact">Kirim</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>