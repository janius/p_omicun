<section class="section-detail">
  <div class="container">
    <div class="headline">
      <h1 class="section-title"><?php echo $contentData['program']->programName ?></h1>
    </div>
    <?php echo $contentData['program']->programDetail ?>
    <hr />
    <a href="<?php echo PATH ?>" class="btn btn-default" role="button"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp; Kembali</a>
  </div>
</section>