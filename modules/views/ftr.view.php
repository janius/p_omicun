<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <p>
            © 2018 QHSE PE. All Rights Reserved. Developed by <a href="https://www.janius.id" target="_blank"> <img src="https://www.kiscoindonesia.com/ast/images/janius.gif" alt=""></a>
        </p>
      </div>
    </div>
  </div>
</footer>