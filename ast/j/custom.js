$(document).ready(function() {
  $('.carousel').carousel({
    interval: 4000,
    pause: 'hover'
  });

  $(".navigation-menu").scrollspy();

  $('.blog-slick').slick({
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 970,
      settings: {
        slidesToShow: 3
      }
    }]
  });

  $('.client-slick').slick({
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 970,
      settings: {
        slidesToShow: 3
      }
    }]
  });
});

$(window).scroll(function() {
  if ($(this).scrollTop() > 100) {
    $('.scroll-up').fadeIn();
  } else {
    $('.scroll-up').fadeOut();
  }
});

$('.scroll-up').click(function() {
  $("html, body").animate({
    scrollTop: 0
  }, 600);
  return false;
});

$(window).scroll(function() {
  "use strict";
  var scroll = $(window).scrollTop();
  if (scroll > 60) {
    $(".navbar").addClass("scroll-fixed-navbar");
  } else {
    $(".navbar").removeClass("scroll-fixed-navbar");
  }
});

$(".navbar-nav li a[href^='#']").on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({
    scrollTop: $(this.hash).offset().top
  }, 1000);
});

map = new GMaps({
  el: '#map',
  scrollwheel: false, // Map Mousewheel scroll disable
  zoom: 17, // Map Zoom
  lat: $( "#map" ).data( "lat" ), // Map Latitudes
  lng: $( "#map" ).data( "long" ) // Map Longitudes
});

map.addMarker({
  lat: $( "#map" ).data( "lat" ), // Map Latitudes
  lng: $( "#map" ).data( "long" ) // Map Longitudes
});

$("#contact_form").parsley({
    successClass: "has-success",
    errorClass: "has-error",
    classHandler: function(e) {
        return e.$element.closest('.form-group');
    },
    errorsWrapper: "<span class='help-block'></span>",
    errorTemplate: "<span></span>"
}).options.requiredMessage = "Kolom harus diisi!";