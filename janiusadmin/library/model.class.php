<?php
class Model{

    public $db;
    protected $tableName;

    public function __construct(){
        $this->db = new Database();
    }

    public function model($modelName){
        require_once ROOT . DS . 'modules' . DS . 'models' . DS . ucfirst($modelName) . 'Model.php';
        $className = ucfirst($modelName) . 'Model';
        $this->$modelName = new $className();
    }

    public function get($params = "") {

        $sql = "SELECT*FROM " . $this->tableName;

        if(is_array($params)) {
            if(isset($params["limit"])) {

                $sql .= " LIMIT " . $params["limit"];
            }
        }

        $this->db->query($sql);

        return $this->db->execute()->toObject();
    }

    public function rows() {
        return $this->db->getAll($this->tableName)->numRows();
    }

    public function rowsWhere($params1, $params2, $operator, $limit) {
        return $this->db->getWhere($this->tableName, $params1, $params2, $operator, $limit)->numRows();
    }

    public function getWhere($params1, $params2, $operator, $limit) {

        return $this->db->getWhere($this->tableName, $params1, $params2, $operator, $limit)->toObject();
    }

    public function getSort($params) {

        return $this->db->getSort($this->tableName, $params)->toObject();
    }

    public function delete($where = array()) {

        return $this->db->delete($this->tableName, $where);
    }

    public function getJoin($tableJoin, $params, $join = "JOIN", $where = array(), $operator, $sort = array(), $limit = array()) {

        $sql = "SELECT*FROM " . $this->tableName;

        if(is_array($tableJoin)) {
            $joinIteration = 0;
            foreach($tableJoin as $table) {

                $sql .= " ". $join ." " . $table . " ";

                $sql .=" ON " . array_keys($params)[$joinIteration] . " = " . array_values($params)[$joinIteration] . " ";

                $joinIteration++;
            }

        } else {
            $sql .= " ". $join ." " . $tableJoin . " ";

            $sql .=" ON " . array_keys($params)[0] . " = " . array_values($params)[0] . " ";
        }

        if(is_array($where)) {

            $sql .= " WHERE ";
            $i = 0;

            foreach($where as $key => $value) {

                if($operator === 'AND' || $operator === 'OR') {
                    $sql .= " " . $key . "='" . $value . "' ";
                } elseif($operator === 'LIKE') {
                    $sql .= " " . $key . " LIKE '" . $value . "'";
                } elseif($operator === 'NOT') {
                    $sql .= " NOT " . $key . "='" . $value . "' ";
                }

                $i++;
                if($i < count($where)) {
                    if($operator === 'AND') {
                        $sql .=" AND ";
                    } elseif($operator === 'OR') {
                        $sql .=" OR ";
                    } elseif($operator === 'LIKE') {
                        $sql .=" OR ";
                    }
                }
            }

        }

        if(is_array($sort)) {
            $sql .= " ORDER BY ";
            $i = 0;

            foreach($sort as $key => $value) {
                $sql .= $key . " " . $value . " ";
                $i++;
                if($i < count($sort)) $sql .= " , ";
            }
        }

        if(is_array($limit)) {
            if(isset($limit["limit"])) {

                $sql .= " LIMIT " . $limit["limit"];
            }
        }

        $this->db->query($sql);

        return $this->db->execute()->toObject();
    }

    public function getQueryManual($sql) {

        $this->db->query($sql);

        return $this->db->execute()->toObject();
    }


    public function rowJoin($tableJoin, $params, $join = "JOIN", $where = "") {

        $sql = "SELECT*FROM " . $this->tableName;

        if(is_array($tableJoin)) {
            $joinIteration = 0;
            foreach($tableJoin as $table) {

                $sql .= " ". $join ." " . $table . " ";

                $sql .=" ON " . array_keys($params)[$joinIteration] . " = " . array_values($params)[$joinIteration] . " ";

                $joinIteration++;
            }

        } else {
            $sql .= " ". $join ." " . $tableJoin . " ";

            $sql .=" ON " . array_keys($params)[0] . " = " . array_values($params)[0] . " ";
        }

        if($where && is_array($where)) {

            $sql .= " WHERE ";
            $i = 0;

            foreach($where as $key => $value) {

                $sql .= " " . $key . "='" . $value . "' ";

                $i++;
                if($i < count($where)) {

                    $sql .=" AND ";
                }
            }

        }

        $this->db->query($sql);

        return $this->db->execute()->numRows();
    }

    public function insert($data = array()) {

        $insert = $this->db->insert($this->tableName, $data);

        if($insert) {
            return true;
        }

        return false;
    }

    public function update($data = array(), $where = array()) {

        $update = $this->db->update($this->tableName, $data, $where);

        if($update) {
            return true;
        }

        return false;
    }
}
?>