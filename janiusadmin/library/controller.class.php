<?php

class Controller{
    protected function view($viewName){
        $view = new View($viewName);
        return $view;
    }

    protected function model($modelName){
        require_once ROOT . DS . 'modules' . DS . 'models' . DS . ucfirst($modelName) . 'Model.php';
        $className = ucfirst($modelName). 'Model';
        $this->$modelName = new $className();
    }
}
?>