<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="<?php echo $contentData["action"] ?>" method="post" enctype="multipart/form-data">        
              <input type="text" name="id" value="<?php echo($contentData["client"] != '') ? $contentData["client"]->clientID : '' ?>" hidden />      
              <div class="form-group">
                <input type="text" name="clientName" class="form-control input-default" placeholder="Nama Klien" value="<?php echo ($contentData["client"] != '') ? $contentData["client"]->clientName : '' ?>"/>
              </div> 
              <div class="form-group">
                <input type="text" name="clientBusinessLine" class="form-control input-default" placeholder="Bidang Bisnis dari Klien" value="<?php echo ($contentData["client"] != '') ? $contentData["client"]->clientBusinessLine : '' ?>"/>
              </div> 
              <div class="form-group">
                <input type="text" name="clientService" class="form-control input-default" placeholder="Pelayan yang telah diberikan/dilakukan" value="<?php echo ($contentData["client"] != '') ? $contentData["client"]->clientService : '' ?>"/>
              </div> 
              <div class="fallback">
                  <input name="clientImage" type="file" multiple />
              </div>
              <?php if ($contentData["client"] != '' && $contentData["client"]->clientImage !== null) { ?>
                <img src="../public/client/<?php echo $contentData["client"]->clientImage ?>" />
              <?php } ?>
              <hr />
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?php echo $contentData["backHref"] ?>" class="btn btn-link">Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>