<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <a href="<?php echo PATH; ?>add-programpublic" class="btn btn-success">Tambah Data</a>
          <div class="table-responsive">
            <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="80%">Nama Program</th>
                  <th width="20%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $no = 1;
                foreach($contentData["program_public"] as $program_public) {
              ?>                                        
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $program_public->programName ?></td>
                  <td>
                    <a href="<?php echo PATH; ?>edit-programpublic-<?php echo $program_public->programPublicID; ?>" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                    <button onclick="confirm_modal('<?php echo PATH ?>delete-programpublic-<?php echo $program_public->programPublicID; ?>')" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>