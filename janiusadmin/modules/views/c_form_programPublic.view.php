<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="<?php echo $contentData["action"] ?>" method="post" enctype="multipart/form-data">        
              <input type="text" name="id" value="<?php echo ($contentData["program_public"] != '') ? $contentData["program_public"]->programPublicID : '' ?>" hidden />      
              <div class="form-group">
                <input type="text" name="programName" class="form-control input-default" placeholder="Nama Program" value="<?php echo ($contentData["program_public"] != '') ? $contentData["program_public"]->programName : '' ?>"/>
              </div>
              <div class="form-group">
                <textarea id="summernote" name="programDetail" data-placeholder="Tulis detail program di sini" data-height="400"><?php echo ($contentData["program_public"] != '') ? $contentData["program_public"]->programDetail : '' ?></textarea >
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?php echo $contentData["backHref"] ?>" class="btn btn-link">Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>