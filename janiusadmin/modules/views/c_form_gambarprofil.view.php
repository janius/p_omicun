<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="<?php echo $contentData["action"] ?>" method="post" enctype="multipart/form-data">        
              <input type="text" name="id" value="<?php echo($contentData["profile_image"] != '') ? $contentData["profile_image"]->profileImageID : '' ?>" hidden />      
              <div class="fallback">
                  <input name="gambar" type="file" multiple />
              </div>
              <?php if ($contentData["profile_image"] != '') { ?>
                <img src="../public/profile/<?php echo $contentData["profile_image"]->image ?>" />
              <?php } ?>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?php echo $contentData["backHref"] ?>" class="btn btn-link">Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>