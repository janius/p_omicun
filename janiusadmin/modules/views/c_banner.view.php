<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <a href="<?php echo PATH; ?>add-banner" class="btn btn-success">Tambah Data</a>
          <div class="table-responsive">
            <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="50%">Gambar</th>
                  <th width="30%">Tulisan</th>
                  <th width="20%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $no = 1;
                foreach($contentData["banner"] as $banner) {
              ?>                                        
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><img style="max-height: 230px" src="<?php echo ROOT_PATH."public/banner/".$banner->image ?>" /></td>
                  <td><?php echo $banner->title ?></td>
                  <td>
                    <a href="<?php echo PATH; ?>edit-banner-<?php echo $banner->bannerID; ?>" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                    <button onclick="confirm_modal('<?php echo PATH ?>delete-banner-<?php echo $banner->bannerID; ?>')" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>