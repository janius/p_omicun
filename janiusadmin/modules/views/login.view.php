<!DOCTYPE html>
<html lang="en">
  <!-- 
Developed by :   

             ██╗ █████╗ ███╗   ██╗██╗██╗   ██╗███████╗   ██╗██████╗ 
             ██║██╔══██╗████╗  ██║██║██║   ██║██╔════╝   ██║██╔══██╗
             ██║███████║██╔██╗ ██║██║██║   ██║███████╗   ██║██║  ██║
        ██   ██║██╔══██║██║╚██╗██║██║██║   ██║╚════██║   ██║██║  ██║
        ╚█████╔╝██║  ██║██║ ╚████║██║╚██████╔╝███████║██╗██║██████╔╝
         ╚════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚══════╝╚═╝╚═╝╚═════╝ 
 __      __   _      ___  _      _ _        _    ___              _   _         
 \ \    / /__| |__  |   \(_)__ _(_) |_ __ _| |  / __|_ _ ___ __ _| |_(_)_ _____ 
  \ \/\/ / -_) '_ \ | |) | / _` | |  _/ _` | | | (__| '_/ -_) _` |  _| \ V / -_)
   \_/\_/\___|_.__/ |___/|_\__, |_|\__\__,_|_|  \___|_| \___\__,_|\__|_|\_/\___|
                           |___/                                                
-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
  <title>Janius Admin</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link href="ast/c/helper.css" rel="stylesheet">
  <link href="ast/c/style.css" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar">

  <div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
  </div>

  <div id="main-wrapper">
    <div class="unix-login">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-lg-4">
            <div class="login-content card">
              <div class="login-form">
                <h4>Login</h4>
                <form method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Nama Pengguna">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Kata Sandi">
                  </div>
                  <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Masuk</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="ast/j/lib/jquery/jquery.min.js"></script>
  <script src="ast/j/lib/bootstrap/js/popper.min.js"></script>
  <script src="ast/j/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="ast/j/jquery.slimscroll.js"></script>
  <script src="ast/j/sidebarmenu.js"></script>
  <script src="ast/j/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
  <script src="ast/j/custom.min.js"></script>
</body>

</html>