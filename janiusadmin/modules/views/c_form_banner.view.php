<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="<?php echo $contentData["action"] ?>" method="post" enctype="multipart/form-data">        
              <input type="text" name="id" value="<?php echo($contentData["banner"] != '') ? $contentData["banner"]->bannerID : '' ?>" hidden />      
              <div class="form-group">
                <input type="text" name="title" class="form-control input-default" placeholder="Tulisan di Banner" value="<?php echo ($contentData["banner"] != '') ? $contentData["banner"]->title : '' ?>"/>
              </div> 
              <div class="fallback">
                  <input name="gambar" type="file" multiple />
              </div>
              <?php if ($contentData["banner"] != '') { ?>
                <img src="../public/banner/<?php echo $contentData["banner"]->image ?>" />
              <?php } ?>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?php echo $contentData["backHref"] ?>" class="btn btn-link">Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>