<?php
    $page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : '';
?>
  <!DOCTYPE html>
  <html lang="en">
  <!-- 
Developed by :   

             ██╗ █████╗ ███╗   ██╗██╗██╗   ██╗███████╗   ██╗██████╗ 
             ██║██╔══██╗████╗  ██║██║██║   ██║██╔════╝   ██║██╔══██╗
             ██║███████║██╔██╗ ██║██║██║   ██║███████╗   ██║██║  ██║
        ██   ██║██╔══██║██║╚██╗██║██║██║   ██║╚════██║   ██║██║  ██║
        ╚█████╔╝██║  ██║██║ ╚████║██║╚██████╔╝███████║██╗██║██████╔╝
         ╚════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚══════╝╚═╝╚═╝╚═════╝ 
 __      __   _      ___  _      _ _        _    ___              _   _         
 \ \    / /__| |__  |   \(_)__ _(_) |_ __ _| |  / __|_ _ ___ __ _| |_(_)_ _____ 
  \ \/\/ / -_) '_ \ | |) | / _` | |  _/ _` | | | (__| '_/ -_) _` |  _| \ V / -_)
   \_/\_/\___|_.__/ |___/|_\__, |_|\__\__,_|_|  \___|_| \___\__,_|\__|_|\_/\___|
                           |___/                                                
-->

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="ast/i/favicon.png">
    <title>Ela - Bootstrap Admin Dashboard Template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet" />
    <link href="ast/c/helper.css" rel="stylesheet">
    <link href="ast/c/style.css" rel="stylesheet">
    <link href="ast/c/myStyle.css" rel="stylesheet">
  </head>

  <body class="fix-header fix-sidebar">
    <div class="preloader">
      <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
      <div class="header">
        <?php
          if($headerViewName != '') {
            $headerView = new View($headerViewName);
            $headerView->bind('headerData', $headerData);
            $headerView->forceRender();
          }
        ?>
      </div>
      <div class="left-sidebar">
        <div class="scroll-sidebar">
          <nav class="sidebar-nav">
            <ul id="sidebarnav">
              <li class="nav-devider"></li>
              <li>
                <a href="<?php echo PATH ?>"><i class="fa fa-tachometer"></i><span class="hide-menu">Dasbor</span></a>
              </li>
              <li>
                <a href="<?php echo ROOT_PATH ?>" target="_blank"><i class="fa fa-tachometer"></i><span class="hide-menu">Lihat Situs</span></a>
              </li>
              <hr />
              <li>
                <a href="<?php echo PATH ?>banner"><i class="fa fa-tachometer"></i><span class="hide-menu">Banner</span></a>
              </li>
              <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Profil </span></a>
                <ul aria-expanded="false" class="collapse">
                  <li><a href="informasi-profil">Informasi </a></li>
                  <li><a href="gambar-profil">Gambar </a></li>
                </ul>
              </li>
              <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Program </span></a>
                <ul aria-expanded="false" class="collapse">
                  <li><a href="program-public">Public </a></li>
                  <li><a href="program-inhouse">Inhouse </a></li>
                </ul>
              </li>
              <li>
                <a href="<?php echo PATH ?>klien"><i class="fa fa-tachometer"></i><span class="hide-menu">Klien</span></a>
              </li>
              <li>
                <a href="<?php echo PATH ?>artikel"><i class="fa fa-tachometer"></i><span class="hide-menu">Artikel</span></a>
              </li>
              <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Kontak </span></a>
                <ul aria-expanded="false" class="collapse">
                  <li><a href="informasi-kontak">Informasi </a></li>
                  <li><a href="pesan-masuk">Pesan Masuk </a></li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="page-wrapper">
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?php echo $contentData['title'] ?></h3>
          </div>
        </div>
        <?php 
          $contentView = new View($contentViewName);
          $contentView->bind('contentData', $contentData);
          $contentView->forceRender();

          if($footerViewName != '') {
            $footertView = new View($footerViewName);
            $footertView->bind('footerData', $footerData);
            $footertView->forceRender();
          }
        ?>
      </div>
    </div>
    <script src="ast/j/lib/jquery/jquery.min.js"></script>
    <script src="ast/j/lib/bootstrap/j/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="ast/j/jquery.slimscroll.js"></script>
    <script src="ast/j/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="ast/j/lib/datatables/datatables.min.js"></script>
    <script src="ast/j/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="ast/j/lib/datatables/datatables-init.js"></script>
    <script src="ast/j/sidebarmenu.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script src="ast/j/scripts.js"></script>
    <script src="ast/j/custom.min.js"></script>

    <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" style="text-align: center">
            <p style="margin-bottom: 0">Anda yakin ingin menghapus data tersebut?</p>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-danger" id="delete_link">Ya</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          </div>
        </div>
      </div>
    </div>
  </body>

  </html>