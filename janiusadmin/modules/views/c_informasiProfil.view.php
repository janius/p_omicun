<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="update-informasiprofil" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <textarea id="summernote" data-height="400" name="description" data-placeholder="Tulis informasi profile perusahaan di sini"><?php echo ($contentData["profile_information"] != '') ? $contentData["profile_information"]->description : '' ?></textarea >
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>