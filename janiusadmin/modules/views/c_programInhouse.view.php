<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <a href="<?php echo PATH; ?>add-programinhouse" class="btn btn-success">Tambah Data</a>
          <div class="table-responsive">
            <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="80%">Nama Program</th>
                  <th width="20%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $no = 1;
                foreach($contentData["program_inhouse"] as $program_inhouse) {
              ?>                                        
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $program_inhouse->programName ?></td>
                  <td>
                    <a href="<?php echo PATH; ?>edit-programinhouse-<?php echo $program_inhouse->programInhouseID; ?>" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                    <button onclick="confirm_modal('<?php echo PATH ?>delete-programinhouse-<?php echo $program_inhouse->programInhouseID; ?>')" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>