<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <a href="<?php echo PATH; ?>add-klien" class="btn btn-success">Tambah Data</a>
          <div class="table-responsive">
            <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="60%">Nama Klien</th>
                  <th width="20%">Top</th>
                  <th width="15%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $no = 1;
                foreach($contentData["client"] as $client) {
              ?>                                        
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $client->clientName ?></td>
                  <td><?php echo ($client->top === 1 ) ? 'true': 'false' ?></td>
                  <td>
                    <a href="<?php echo PATH; ?>edit-klien-<?php echo $client->clientID; ?>" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                    <button onclick="confirm_modal('<?php echo PATH ?>delete-klien-<?php echo $client->clientID; ?>')" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>