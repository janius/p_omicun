<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="save-informasi-kontak" method="post">
              <div class="form-group">
                <textarea class="form-control" name="address" rows="4" placeholder="Alamat"><?php echo $contentData["kontak"]->address ?></textarea>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="email" class="form-control input-default" placeholder="Email" value="<?php echo $contentData["kontak"]->email ?>"/>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="mobileNumber" class="form-control input-default" placeholder="Nomor Handphone" value="<?php echo $contentData["kontak"]->mobileNumber ?>" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="mapLat" class="form-control input-default" placeholder="Latitude Gmaps" value="<?php echo $contentData["kontak"]->mapLat ?>" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="mapLong" class="form-control input-default" placeholder="Longitude Gmaps" value="<?php echo $contentData["kontak"]->mapLong ?>" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>