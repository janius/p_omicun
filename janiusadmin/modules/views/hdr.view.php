<nav class="navbar top-navbar navbar-expand-md navbar-light">
  <div class="navbar-header">
    <a class="navbar-brand" href="index.html">
                <b><img src="ast/i/logo.png" alt="homepage" class="dark-logo" /></b>
                <span><img src="ast/i/logo-text.png" alt="homepage" class="dark-logo" /></span>
            </a>
  </div>
  <div class="navbar-collapse">
    <ul class="navbar-nav mr-auto mt-md-0">
    </ul>
    <ul class="navbar-nav my-lg-0">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="ast/i/users/5.jpg" alt="user" class="profile-pic" /></a>
        <div class="dropdown-menu dropdown-menu-right animated zoomIn">
          <ul class="dropdown-user">
            <li><a href="#"><i class="fa fa-lock"></i> Ganti Kata Sandi</a></li>
            <li><a href="<?php echo PATH ?>logout"><i class="fa fa-power-off"></i> keluar</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>