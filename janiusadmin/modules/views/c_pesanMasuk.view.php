<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="23%">Nama</th>
                  <th width="22%">Email</th>
                  <th width="50%">Pesan</th>
                </tr>
              </thead>
              <tbody>
              <?php
                $no = 1;
                foreach($contentData["pesan_masuk"] as $pesan_masuk) {
              ?>                                        
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $pesan_masuk->name ?></td>
                  <td><?php echo $pesan_masuk->email ?></td>
                  <td><?php echo $pesan_masuk->message ?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>