<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="basic-form">
            <form action="<?php echo $contentData["action"] ?>" method="post" enctype="multipart/form-data">        
              <input type="text" name="id" value="<?php echo($contentData["artikel"] != '') ? $contentData["artikel"]->articleID : '' ?>" hidden />      
              <div class="form-group">
                <input type="text" name="articleTitle" class="form-control input-default" placeholder="Judul Artikel" value="<?php echo ($contentData["artikel"] != '') ? $contentData["artikel"]->articleTitle : '' ?>"/>
              </div> 
              <div class="fallback">
                  <input name="articleImage" type="file" multiple />
              </div>
              <?php if ($contentData["artikel"] != '') { ?>
                <img width="200" src="../public/article/<?php echo $contentData["artikel"]->articleImage ?>" />
              <?php } ?>
              <div class="form-group">
                <textarea id="summernote" data-height="400" name="articleDetail" data-placeholder="Tulis isi artikel di sini"><?php echo ($contentData["artikel"] != '') ? $contentData["artikel"]->articleDetail : '' ?></textarea >
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?php echo $contentData["backHref"] ?>" class="btn btn-link">Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>