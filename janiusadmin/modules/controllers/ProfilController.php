<?php

use \modules\controllers\MainController;

class ProfilController extends MainController {

  public function seeInformasiProfil() {

    $this->model('ProfileInformation');

    $dataProfileInformation = $this->ProfileInformation->get();

  	$this->template('tmplt', 'hdr', 'c_informasiProfil', 'ftr', array(), 
		array(
  		 	'title' => 'Informasi Profil',
  		 	'profile_information' => $dataProfileInformation[0]
  		),
	array());
    
  }

    public function listGambarProfil() {

      $this->model('ProfileImage');

      $dataProfileImage = $this->ProfileImage->get();

    	$this->template('tmplt', 'hdr', 'c_gambarProfil', 'ftr', array(), 
  		array(
    		 	'title' => 'Gambar Profil',
    		 	'profile_image' => $dataProfileImage
    		),
  	array());      
    }

    public function updateInformationProfile() {

      $this->model('ProfileInformation');

      $description = $_POST["description"];

      $update = $this->ProfileInformation->update(
        array(
          'description'   => $description
        ), 1
      );  

      if($update) {

        header('Location: informasi-profil');
        break;
      } 
    }

    public function formGambarProfil() {

      $dataGambarProfil = '';
      $action = 'insert-gambarprofil';
      $title = 'Tambar Gambar Profil';

      if(isset($_GET["id"])) {

        $this->model('ProfileImage');

        $id = $_GET["id"];

        $dataGambarProfil = $this->ProfileImage->getWhere(array(
          'profileImageID' => $id
        ), '', 'AND', '')[0];

        $action = 'update-gambarprofil';
        $title = 'Edit Gambar Profil';
      } 

      $this->template('tmplt', 'hdr', 'c_form_gambarProfil', 'ftr', array(), 
        array(
            'title' => $title,
            'backHref' => 'gambar-profil',
            'action' => $action,
            'profile_image' => $dataGambarProfil
          ),
      array());    
    }

    public function insertGambarProfil() {

      $this->model('ProfileImage');

      $gambar = isset($_FILES["gambar"]) ? $_FILES["gambar"] : "";
      
      $imageName = '';

      if($gambar["name"]) {

          $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

          $imageName = date("h_i_s_Y_m_d_") . 'gambar-profil.jpg';

          move_uploaded_file($gambar["tmp_name"], '../public/profile/' . $imageName);
      }

      $insert = $this->ProfileImage->insert(
        array(
          'image'   => $imageName
        )
      );  

      if($insert) {

        header('Location: gambar-profil');
        break;
      } 
    }

    public function updateGambarProfil() {

      $this->model('ProfileImage');

      $id = $_POST["id"];

      $profile_image = $this->ProfileImage->getWhere(array(
          'profileImageID' => $id
        ), '', 'AND', '')[0];

      $gambar = isset($_FILES["gambar"]) ? $_FILES["gambar"] : "";

      $updateArray = [];

      if($gambar["name"]) {

          $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

          $imageName = date("h_i_s_Y_m_d_") . 'gambar-profil.jpg';

          if(file_exists('../public/profile/' . $profile_image->image)) {

              unlink('../public/profile/' . $profile_image->image);
          }

          move_uploaded_file($gambar["tmp_name"], '../public/profile/' . $imageName);

          $updateArray['image'] = $imageName;
      }

      $update = $this->ProfileImage->update(
        $updateArray, 
        array(
          'profileImageID' => $id
        )
      );  

      if($update) {

        header('Location: gambar-profil');
        break;
      } 
    }

    public function deleteGambarProfil() {

      $this->model('ProfileImage');

      $id = $_GET["id"];

      $delete = $this->ProfileImage->delete(array(
        'profileImageID' => $id
      ));

      if($delete) {

        header('Location: gambar-profil');
        break;
      } 
    }
}
?>