<?php

use \modules\controllers\MainController;

class BannerController extends MainController {

  public function index() {

    $this->model('Banner');

    $dataBanner = $this->Banner->get();

  	$this->template('tmplt', 'hdr', 'c_banner', 'ftr', array(), 
		array(
  		 	'title' => 'Banner',
  		 	'banner' => $dataBanner
  		),
	   array());    
  }

  public function formBanner() {

    $dataBanner = '';
    $action = 'insert-banner';
    $title = 'Tambar Banner';

    if(isset($_GET["id"])) {

      $this->model('Banner');

      $id = $_GET["id"];

      $dataBanner = $this->Banner->getWhere(array(
        'bannerID' => $id
      ), '', 'AND', '')[0];

      $action = 'update-banner';
      $title = 'Edit Banner';
    } 

    $this->template('tmplt', 'hdr', 'c_form_banner', 'ftr', array(), 
      array(
          'title' => $title,
          'backHref' => 'banner',
          'action' => $action,
          'banner' => $dataBanner
        ),
    array());    
  }

  public function insertBanner() {

    $this->model('Banner');

    $title = $_POST["title"];
    $gambar = isset($_FILES["gambar"]) ? $_FILES["gambar"] : "";
    
    $imageName = '';

    if($gambar["name"]) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . 'banner.jpg';

        move_uploaded_file($gambar["tmp_name"], '../public/banner/' . $imageName);
    }

    $insert = $this->Banner->insert(
      array(
        'title'   => $title,
        'image'   => $imageName
      )
    );  

    if($insert) {

      header('Location: banner');
      break;
    } 
  }

  public function updateBanner() {

    $this->model('Banner');

    $id = $_POST["id"];

    $title = $_POST["title"];
    $gambar = isset($_FILES["gambar"]) ? $_FILES["gambar"] : "";

    $banner = $this->Banner->getWhere(array(
        'bannerID' => $id
      ), '', 'AND', '')[0];

    $updateArray = array(
      'title'   => $title,      
    );

    if($gambar['name']) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . 'banner.jpg';

        if(file_exists('../public/banner/' . $banner->image)) {

            unlink('../public/banner/' . $banner->image);
        }

        move_uploaded_file($gambar["tmp_name"], '../public/banner/' . $imageName);

        $updateArray['image'] = $imageName;
    }

    $update = $this->Banner->update(
      $updateArray, 
      array(
        'bannerID' => $id
      )
    );  

    if($update) {

      header('Location: banner');
      break;
    } 
  }

  public function deleteBanner() {

    $this->model('Banner');

    $id = $_GET["id"];

    $delete = $this->Banner->delete(array(
      'bannerID' => $id
    ));

    if($delete) {

      header('Location: banner');
      break;
    } 
  }
}
?>