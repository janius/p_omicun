<?php

use \modules\controllers\MainController;

class KontakController extends MainController {

  public function index() {

    $this->model('Contact');

    $dataKontak = $this->Contact->get();

  	$this->template('tmplt', 'hdr', 'c_informasiKontak', 'ftr', array(), 
		array(
  		 	'title' => 'Informasi Kontak',
        'kontak' => $dataKontak[0]
  		),
	   array());    
  }

  public function pesanMasuk() {

    $this->model('IncomingMessage');

    $dataPesanMasuk = $this->IncomingMessage->get();

  	$this->template('tmplt', 'hdr', 'c_pesanMasuk', 'ftr', array(), 
		array(
  		 	'title' => 'Pesan Masuk',
        'pesan_masuk' => $dataPesanMasuk
  		),
	   array());    
  }

  public function saveInformasiKontak() {

    $this->model('Contact');

    $address = $_POST["address"];
    $email = $_POST["email"];
    $mobileNumber = $_POST["mobileNumber"];
    $mapLat = $_POST["mapLat"];
    $mapLong = $_POST["mapLong"];

    $update = $this->Contact->update(
      array(
        'address'   => $address,
        'email'   => $email,
        'mobileNumber' => $mobileNumber,
        'mapLat'   => $mapLat,
        'mapLong'   => $mapLong
      ),
      1
    );  

    if($update) {

      header('Location: informasi-kontak');
      break;
    } 
  }
}
?>