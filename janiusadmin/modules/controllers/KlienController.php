<?php

use \modules\controllers\MainController;

class KlienController extends MainController {

  public function index() {

    $this->model('Client');

    $dataClient = $this->Client->getWhere(
      '', 
      array(
        'clientName' => 'ASC'
      ), 'AND', '');

  	$this->template('tmplt', 'hdr', 'c_klien', 'ftr', array(), 
		array(
  		 	'title' => 'Klien',
  		 	'client' => $dataClient
  		),
	   array());    
  }

  public function formKlien() {

    $dataKlien = '';
    $action = 'insert-klien';
    $title = 'Tambar Klien';

    if(isset($_GET["id"])) {

      $this->model('Client');

      $id = $_GET["id"];

      $dataKlien = $this->Client->getWhere(array(
        'clientID' => $id
      ), '', 'AND', '')[0];

      $action = 'update-klien';
      $title = 'Edit Klien';
    } 

    $this->template('tmplt', 'hdr', 'c_form_klien', 'ftr', array(), 
      array(
          'title' => $title,
          'backHref' => 'klien',
          'action' => $action,
          'client' => $dataKlien
        ),
    array());    
  }

  public function insertKlien() {

    $this->model('Client');

    $clientName = $_POST["clientName"];
    $clientBusinessLine = $_POST["clientBusinessLine"];
    $clientService = $_POST["clientService"];
    $clientImage = isset($_FILES["clientImage"]) ? $_FILES["clientImage"] : "";
    
    $imageName = '';

    if($clientImage["name"]) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . str_replace($find,"_", $clientName) . '.jpg';

        move_uploaded_file($clientImage["tmp_name"], '../public/client/' . $imageName);
    }

    $insert = $this->Client->insert(
      array(
        'clientName'   => $clientName,
        'clientNameSlug'   => $this->slugify($clientName),
        'clientBusinessLine'   => $clientBusinessLine,
        'clientService'   => $clientService,
        'clientImage'   => $imageName
      )
    );  

    if($insert) {

      header('Location: klien');
      break;
    } 
  }

  public function updateKlien() {

    $this->model('Client');

    $id = $_POST["id"];

    $clientName = $_POST["clientName"];
    $clientBusinessLine = $_POST["clientBusinessLine"];
    $clientService = $_POST["clientService"];
    $clientImage = isset($_FILES["clientImage"]) ? $_FILES["clientImage"] : "";

    $klien = $this->Client->getWhere(array(
        'clientID' => $id
      ), '', 'AND', '')[0];

    $updateArray = array(
      'clientName'   => $clientName,
      'clientNameSlug'   => $this->slugify($clientName),
      'clientBusinessLine'   => $clientBusinessLine,
      'clientService'   => $clientService
    );

    if($clientImage['name']) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . str_replace($find,"_", $clientName) . '.jpg';

        if(file_exists('../public/client/' . $klien->clientImage)) {

            unlink('../public/client/' . $klien->clientImage);
        }

        move_uploaded_file($clientImage["tmp_name"], '../public/client/' . $imageName);

        $updateArray['clientImage'] = $imageName;
    }

    $update = $this->Client->update(
      $updateArray, 
      array(
        'clientID' => $id
      )
    );  

    if($update) {

      header('Location: klien');
      break;
    } 
  }

  public function deleteKlien() {

    $this->model('Client');

    $id = $_GET["id"];

    $delete = $this->Client->delete(array(
      'clientID' => $id
    ));

    if($delete) {

      header('Location: klien');
      break;
    } 
  }

  function slugify($text) {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '-');

      // remove duplicated - symbols
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
  }
}
?>