<?php

namespace modules\controllers;
use \Controller;

class MainController extends Controller {

    protected $login;

    public function __construct() {

        $this->login = isset($_SESSION["login"]) ? $_SESSION["login"] : '';

        if(!$this->login) {

            header('Location:'.PATH."login");
        }
    }

    protected function template($templateName, $headerViewName, $contentViewName, $footerViewName, $headerData = array(), $contentData = array(), $footerData = array()) {

        $view = $this->view($templateName);

        $page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : '';

        $view->bind('headerViewName', $headerViewName);
        $view->bind('contentViewName', $contentViewName);
        $view->bind('footerViewName', $footerViewName);

        $view->bind('headerData', $headerData);
        $view->bind('contentData', $contentData);
        $view->bind('footerData', $footerData);
    }

    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicated - symbols
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
          return 'n-a';
        }

        return $text;
    }

}
?>