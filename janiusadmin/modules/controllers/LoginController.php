<?php

class LoginController extends Controller {

    public function index() {

        $login = isset($_SESSION["login"]) ? $_SESSION["login"] : "";

        if($login) {

            header('Location:'.PATH);
            break;
        }

        $message = array();

        if($_SERVER["REQUEST_METHOD"] == "POST") {

            $username = isset($_POST["username"]) ? $_POST["username"] : "";
            $password = isset($_POST["password"]) ? $_POST["password"] : "";

            $this->model('User');

            $pengguna = $this->User->getWhere(array(
                'username' => $username,
                'password' => hash('sha256', $password)
            ),'','AND', '');

            if(count($pengguna) > 0) {

                $_SESSION["login"] = $pengguna[0];

                header('Location:'.PATH);
                break;

            }
        }
        $view = $this->view('login');
    }



    public function logout() {

        unset($_SESSION["login"]);

        header('Location:'.PATH);
    }

}
?>