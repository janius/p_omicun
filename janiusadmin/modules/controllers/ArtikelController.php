<?php

use \modules\controllers\MainController;

class ArtikelController extends MainController {

  public function index() {

    $this->model('Article');

    $dataArticle = $this->Article->get();

  	$this->template('tmplt', 'hdr', 'c_artikel', 'ftr', array(), 
		array(
  		 	'title' => 'Artikel',
  		 	'article' => $dataArticle
  		),
	array());

    
  }

  public function formArtikel() {

    $dataArticle = '';
    $action = 'insert-artikel';
    $title = 'Tambar Artikel';

    if(isset($_GET["id"])) {

      $this->model('Article');

      $id = $_GET["id"];

      $dataArticle = $this->Article->getWhere(array(
        'articleID' => $id
      ), '', 'AND', '')[0];

      $action = 'update-artikel';
      $title = 'Edit Artikel';
    } 

    $this->template('tmplt', 'hdr', 'c_form_artikel', 'ftr', array(), 
      array(
          'title' => $title,
          'backHref' => 'artikel',
          'action' => $action,
          'artikel' => $dataArticle
        ),
    array());    
  }

  public function insertArtikel() {

    $this->model('Article');

    $articleTitle = $_POST["articleTitle"];
    $articleDetail = $_POST["articleDetail"];
    $articleImage = isset($_FILES["articleImage"]) ? $_FILES["articleImage"] : "";
    
    $imageName = '';

    if($articleImage["name"]) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . 'artikel.jpg';

        move_uploaded_file($articleImage["tmp_name"], '../public/article/' . $imageName);
    }

    $insert = $this->Article->insert(
      array(
        'articleTitle'   => $articleTitle,
        'articleTitleSlug'   => $this->slugify($articleTitle),
        'articleDate'   => date("Y-m-d"),
        'articleDetail'   => $articleDetail,
        'articleImage'   => $imageName,
        'articleView'   => 0
      )
    );  

    if($insert) {

      header('Location: artikel');
      break;
    } 
  }

  public function updateArtikel() {

    $this->model('Article');

    $id = $_POST["id"];

    $articleTitle = $_POST["articleTitle"];
    $articleDetail = $_POST["articleDetail"];
    $articleImage = isset($_FILES["articleImage"]) ? $_FILES["articleImage"] : "";

    $artikel = $this->Article->getWhere(array(
        'articleID' => $id
      ), '', 'AND', '')[0];

    $updateArray = array(
      'articleTitle'   => $articleTitle,
      'articleTitleSlug'   => $this->slugify($articleTitle),
      'articleDetail'   => $articleDetail
    );

    if($articleImage['name']) {

        $find = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

        $imageName = date("h_i_s_Y_m_d_") . 'artikel.jpg';

        if(file_exists('../public/article/' . $artikel->articleImage)) {

            unlink('../public/article/' . $artikel->articleImage);
        }

        move_uploaded_file($articleImage["tmp_name"], '../public/article/' . $imageName);

        $updateArray['articleImage'] = $imageName;
    }

    $update = $this->Article->update(
      $updateArray, 
      array(
        'articleID' => $id
      )
    );  

    if($update) {

      header('Location: artikel');
      break;
    } 
  }

  public function deleteArtikel() {

    $this->model('Article');

    $id = $_GET["id"];

    $delete = $this->Article->delete(array(
      'articleID' => $id
    ));

    if($delete) {

      header('Location: artikel');
      break;
    } 
  }

}
?>