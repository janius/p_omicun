<?php

use \modules\controllers\MainController;

class ProgramController extends MainController {

  public function programPublic() {

    $this->model('ProgramPublic');

    $dataProgramPublic = $this->ProgramPublic->getSort(array('created' => 'DESC'));

    $this->template('tmplt', 'hdr', 'c_programPublic', 'ftr', array(), 
  		array(
          'title' => 'Program Public',
    		 	'program_public' => $dataProgramPublic
    		),
  	array());    
  }

  public function programInhouse() {

    $this->model('ProgramInhouse');

    $dataProgramInhouse = $this->ProgramInhouse->getSort(array('created' => 'DESC'));

    $this->template('tmplt', 'hdr', 'c_programInhouse', 'ftr', array(), 
  		array(
    		 	'title' => 'Program In-House',
          'program_inhouse' => $dataProgramInhouse
    		),
  	array());    
  }

  public function formProgramPublic() {

    $dataProgramPublic = '';
    $action = 'insert-programpublic';

    if(isset($_GET["id"])) {

      $this->model('ProgramPublic');

      $id = $_GET["id"];

      $dataProgramPublic = $this->ProgramPublic->getWhere(array(
        'programPublicID' => $id
      ), '', 'AND', '')[0];

      $action = 'update-programpublic';
    } 

    $this->template('tmplt', 'hdr', 'c_form_programPublic', 'ftr', array(), 
      array(
          'title' => 'Program Public',
          'backHref' => 'program-public',
          'action' => $action,
          'program_public' => $dataProgramPublic
        ),
    array());    
  }

  public function insertProgramPublic() {

    $this->model('ProgramPublic');

    $programName = $_POST["programName"];
    $programDetail = $_POST["programDetail"];

    $insert = $this->ProgramPublic->insert(
      array(
        'programName'   => $programName,
        'programNameSlug'   => $this->slugify($programName),
        'programDetail'   => $programDetail,
        'created'   => date("Y-m-d H:i:s")
      )
    );  

    if($insert) {

      header('Location: program-public');
      break;
    } 
  }

  public function updateProgramPublic() {

    $this->model('ProgramPublic');

    $id = $_POST["id"];
    $programName = $_POST["programName"];
    $programDetail = $_POST["programDetail"];

    $update = $this->ProgramPublic->update(
      array(
        'programName'   => $programName,
        'programNameSlug'   => $this->slugify($programName),
        'programDetail'   => $programDetail,
        'created'   => date("Y-m-d H:i:s")
      ), 
      array(
        'programPublicID' => $id
      )
    );

    if($update) {

      header('Location: program-public');
      break;
    } 
  }

  public function deleteProgramPublic() {

    $this->model('ProgramPublic');

    $id = $_GET["id"];

    $delete = $this->ProgramPublic->delete(array(
      'programPublicID' => $id
    ));

    if($delete) {

      header('Location: program-public');
      break;
    } 
  }

  public function formProgramInhouse() {

    $dataProgramInhouse = '';
    $action = 'insert-programinhouse';

    if(isset($_GET["id"])) {

      $this->model('ProgramInhouse');

      $id = $_GET["id"];

      $dataProgramInhouse = $this->ProgramInhouse->getWhere(array(
        'programInhouseID' => $id
      ), '', 'AND', '')[0];

      $action = 'update-programinhouse';
    } 

    $this->template('tmplt', 'hdr', 'c_form_programInhouse', 'ftr', array(), 
      array(
          'title' => 'Program Inhouse',
          'backHref' => 'program-inhouse',
          'action' => $action,
          'program_inhouse' => $dataProgramInhouse
        ),
    array());    
  }

  public function insertProgramInhouse() {

    $this->model('ProgramInhouse');

    $programName = $_POST["programName"];
    $programDetail = $_POST["programDetail"];

    $insert = $this->ProgramInhouse->insert(
      array(
        'programName'   => $programName,
        'programNameSlug'   => $this->slugify($programName),
        'programDetail'   => $programDetail,
        'created'   => date("Y-m-d H:i:s")
      )
    );  

    if($insert) {

      header('Location: program-inhouse');
      break;
    } 
  }

  public function updateProgramInhouse() {

    $this->model('ProgramInhouse');

    $id = $_POST["id"];
    $programName = $_POST["programName"];
    $programDetail = $_POST["programDetail"];

    $update = $this->ProgramInhouse->update(
      array(
        'programName'   => $programName,
        'programNameSlug'   => $this->slugify($programName),
        'programDetail'   => $programDetail,
        'created'   => date("Y-m-d H:i:s")
      ), 
      array(
        'programInhouseID' => $id
      )
    );  

    if($update) {

      header('Location: program-inhouse');
      break;
    } 
  }

  public function deleteProgramInhouse() {

    $this->model('ProgramInhouse');

    $id = $_GET["id"];

    $delete = $this->ProgramInhouse->delete(array(
      'programInhouseID' => $id
    ));

    if($delete) {

      header('Location: program-inhouse');
      break;
    } 
  }
}
?>