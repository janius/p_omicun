<?php
$page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : '';

date_default_timezone_set('Asia/Jakarta');

define('ROOT_PATH', 'http://localhost/omicun/');
define('PATH', 'http://localhost/omicun/janiusadmin/');
define('SITE_URL', PATH . 'index.php');
define('POSITION_URL', PATH . '?page=' . $page);

define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'db_omicun');
?>
